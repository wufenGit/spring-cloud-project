package com.project.service.service.feign;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/testFeignService")
public class TestFeignService {

    @GetMapping(value = "/helloFeign")
    public String helloFeign(@RequestParam("str") String str) {
        return str;
    }
}
