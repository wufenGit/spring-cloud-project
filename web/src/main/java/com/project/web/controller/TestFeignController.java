package com.project.web.controller;

import com.project.web.service.TestFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/testFeign")
public class TestFeignController {
    @Autowired
    private TestFeignService testFeignService;

    @GetMapping(value = "/helloFeign")
    public String helloFeign(@RequestParam("str") String str) {
        return testFeignService.helloFeign(str);
    }

}
