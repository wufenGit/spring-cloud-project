package com.project.web.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "project-service", fallback = TestFeignService.TestHystrix.class)
public interface TestFeignService {

    @GetMapping(value = "/testFeignService/helloFeign")
    String helloFeign(@RequestParam("str") String str);

    @Component
    public class TestHystrix implements TestFeignService {

        @Override
        public String helloFeign(String str) {
            return "I am fine just not happy";
        }
    }

}
