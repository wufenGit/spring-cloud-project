package com.eproject.common.utils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by boot on 2020/6/17.
 */
@Data
public class ReturnResult<T> implements Serializable {
    @ApiModelProperty(value = "状态码")
    private int code;//状态码
    @ApiModelProperty(value = "信息")
    private String msg;//信息
    @ApiModelProperty(value = "数据")
    private T data;//数据
}
