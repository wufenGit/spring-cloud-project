package com.eproject.common.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by boot on 2020/6/17.
 */
@ApiModel
@Data
public class PageUtils<T> implements Serializable {
    @ApiModelProperty(value = "当前页")
    private Integer currentPage;//当前页
    @ApiModelProperty(value = "页码")
    private Integer pageNo = 1;//页码
    @ApiModelProperty(value = "每页条数")
    private Integer pageSize = 15;//每页条数
    @ApiModelProperty(value = "总页数")
    private long totalPage;//总页数
    @ApiModelProperty(value = "总条数")
    private long totalCount;//总条数
    private Collection<T> currentList;


    public Integer getPageNo() {
        return (pageNo - 1) * pageSize;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public long getTotalPage() {
        return totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }
}
